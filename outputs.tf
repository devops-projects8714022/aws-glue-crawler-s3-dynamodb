output "glue_crawler_name" {
  description = "The name of the Glue Crawler"
  value       = aws_glue_crawler.s3_dynamodb_crawler.name
}

output "glue_service_role_arn" {
  description = "The ARN of the Glue service role"
  value       = aws_iam_role.glue_service_role.arn
}
