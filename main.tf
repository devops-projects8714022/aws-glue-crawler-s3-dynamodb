provider "aws" {
  region = var.region
}

resource "aws_iam_role" "glue_service_role" {
  name = "glue_service_role"
  
  assume_role_policy = jsonencode({
    Version = "2012-10-17",
    Statement = [
      {
        Action = "sts:AssumeRole",
        Principal = {
          Service = "glue.amazonaws.com"
        },
        Effect = "Allow",
        Sid    = ""
      }
    ]
  })
}

resource "aws_iam_role_policy_attachment" "glue_service_policy" {
  role       = aws_iam_role.glue_service_role.name
  policy_arn = "arn:aws:iam::aws:policy/service-role/AWSGlueServiceRole"
}

resource "aws_glue_crawler" "s3_dynamodb_crawler" {
  name = var.crawler_name
  role = aws_iam_role.glue_service_role.arn
  database_name = var.database_name

  s3_target {
    path = var.s3_path
  }

  dynamodb_target {
    path = var.dynamodb_table_name
  }

  schedule = var.schedule

  schema_change_policy {
    update_behavior = "UPDATE_IN_DATABASE"
    delete_behavior = "DEPRECATE_IN_DATABASE"
  }
}
