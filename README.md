## Модуль Terraform для развертывания AWS Glue Crawler с целью сканирования S3 и DynamoDB

Этот модуль Terraform предназначен для развертывания AWS Glue Crawler, который будет сканировать данные, хранящиеся в S3 и DynamoDB. Модуль включает в себя создание необходимых ресурсов, таких как роль IAM для Glue и сам Glue Crawler, с использованием предоставленных переменных.

### Переменные

- **region**: Регион AWS, в котором будет развернут Glue Crawler.
- **crawler_name**: Имя для Glue Crawler.
- **database_name**: Имя базы данных в Glue Data Catalog.
- **s3_path**: Путь к данным в S3, которые нужно сканировать.
- **dynamodb_table_name**: Имя таблицы DynamoDB, которую нужно сканировать.
- **schedule**: Расписание для запуска Glue Crawler в формате cron (по умолчанию ежедневно в 12:00 UTC).

### Пример использования

Для использования этого модуля в вашей конфигурации Terraform, создайте файл `main.tf` в корневой директории вашего проекта и добавьте следующий код:

```hcl
module "glue_crawler" {
  source = "./terraform-aws-glue-crawler"

  region                = "us-west-2"
  crawler_name          = "my-glue-crawler"
  database_name         = "my_glue_database"
  s3_path               = "s3://my-bucket/data/"
  dynamodb_table_name   = "my_dynamodb_table"
  schedule              = "cron(0 12 * * ? *)"
}

output "glue_crawler_name" {
  value = module.glue_crawler.glue_crawler_name
}

output "glue_service_role_arn" {
  value = module.glue_crawler.glue_service_role_arn
}
```

### Выходные данные

- **glue_crawler_name**: Имя созданного Glue Crawler.
- **glue_service_role_arn**: ARN роли IAM, используемой Glue Crawler.

### Описание процесса

1. **Создание роли IAM для Glue**: Роль IAM создается с необходимыми разрешениями для работы с AWS Glue.
2. **Настройка Glue Crawler**: Glue Crawler настраивается для сканирования данных в указанных хранилищах S3 и DynamoDB.
3. **Создание расписания**: Задается расписание для регулярного запуска Glue Crawler с использованием cron-выражений.
4. **Обновление метаданных**: Glue Crawler автоматически обновляет схемы и метаданные в Glue Data Catalog, обеспечивая актуальность данных.

### Дополнительная информация

- **AWS Glue**: Полностью управляемый сервис ETL (Extract, Transform, Load), который упрощает подготовку и загрузку данных для аналитики.
- **Glue Data Catalog**: Централизованный каталог метаданных, который используется для хранения информации о данных.

Этот модуль упрощает процесс настройки и управления Glue Crawler для автоматизации процесса сканирования и каталогизации данных, обеспечивая их доступность для последующей обработки и анализа в AWS.