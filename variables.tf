variable "region" {
  description = "The AWS region to deploy the Glue Crawler"
  type        = string
}

variable "crawler_name" {
  description = "The name of the Glue Crawler"
  type        = string
}

variable "database_name" {
  description = "The Glue Data Catalog database name"
  type        = string
}

variable "s3_path" {
  description = "The S3 path for the Glue Crawler"
  type        = string
}

variable "dynamodb_table_name" {
  description = "The DynamoDB table name for the Glue Crawler"
  type        = string
}

variable "schedule" {
  description = "The schedule for the Glue Crawler in cron expression format"
  type        = string
  default     = "cron(0 12 * * ? *)" # Default to run daily at 12 PM UTC
}
